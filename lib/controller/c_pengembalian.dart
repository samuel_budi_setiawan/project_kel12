import 'package:get/get.dart';
import 'package:project_kel_9/model/pengembalian.dart';

class CUser extends GetxController {
  Rx<Pengembalian> _user = Pengembalian().obs;

  Pengembalian get user => _user.value;

  void setUser(Pengembalian dataUser) => _user.value = dataUser;
}
