import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Asset {
  static Color colorPrimaryDark = Color.fromARGB(255, 163, 255, 5);
  static Color colorPrimary = Color.fromARGB(255, 218, 73, 0);
  static Color colorSecondary = Color.fromARGB(255, 147, 252, 1);
  static Color colorAccent = Color.fromARGB(255, 115, 18, 32);
}
